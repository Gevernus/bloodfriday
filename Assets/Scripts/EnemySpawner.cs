﻿using System.Collections;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private int maxSpawnLimit = 100;
    [SerializeField] private Enemy[] enemies;
    [SerializeField] private StartPoint[] startPositions;
    [SerializeField] private int scorePerEnemy = 20;
    [SerializeField] private int scorePerMiss = -10;
    [SerializeField] private int firstEnemyScore = 60;
    [SerializeField] private int[] stagesSpawn;

    private bool _firstKilled;
    private int _spawned;

    private void Awake()
    {
        Core.Register(this);
    }

    public void StartSpawn()
    {
        StartCoroutine(SpawnRoutine());
    }

    public void StopSpawn()
    {
        StopCoroutine(SpawnRoutine());
    }

    public Enemy GetClosestEnemy(Vector3 point)
    {
        var min = float.MaxValue;
        Enemy result = default;
        foreach (var enemy in enemies)
        {
            var sqrMagnitude = Vector3.SqrMagnitude(enemy.transform.position - point);
            if (sqrMagnitude < min)
            {
                min = sqrMagnitude;
                result = enemy;
            }
        }

        return result;
    }

    private void EnemyKilled(int type)
    {
        Core.Get<ScoreController>().AddScore(type);
        _firstKilled = true;
    }

    public int CountEnemies()
    {
        return enemies.Count(enemy => enemy.isActiveAndEnabled);
    }

    private IEnumerator SpawnRoutine()
    {
        while (_spawned < maxSpawnLimit)
        {
            var enemyToSpawn = enemies.Where(enemy1 => !enemy1.isActiveAndEnabled).ToList();
            var enemy = enemyToSpawn[Random.Range(0, enemyToSpawn.Count)];
            if (enemy == null || enemy.isActiveAndEnabled)
            {
                yield return new WaitForSeconds(1);
                continue;
            }

            var startPosition = startPositions[Random.Range(0, startPositions.Length)];
            var position = (Vector2) startPosition.BeginPoint.position;
            var endPosition = (Vector2) startPosition.EndPoint.position;
            var direction = 2 * endPosition - position;

            var beatController = Core.Get<BeatController>();
            enemy.Init(position, direction,
                (beatController.BeatDelay - beatController.FromLastBeat + beatController.BeatDelay) * 2f, EnemyKilled);
            _spawned++;
            for (var i = 0; i < stagesSpawn.Length; i++)
            {
                if (_spawned == stagesSpawn[i])
                {
                    Core.Get<EyesController>().SetStage(i);
                    break;
                }
            }
            
            yield return new WaitForSeconds(beatController.BeatDelay);
        }
    }
}