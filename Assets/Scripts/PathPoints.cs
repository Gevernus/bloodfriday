﻿using UnityEngine;

public class PathPoints : MonoBehaviour
{
    [SerializeField] public Transform[] points;
    [SerializeField] public float pointDuration;
}