﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private Dictionary<KeyCode, List<Action>> _listeners = new Dictionary<KeyCode, List<Action>>();
    private List<Action> _buttonActions = new List<Action>();

    private void Awake()
    {
        Core.Register(this);
    }

    public void AddListener(KeyCode button, Action listener)
    {
        if (!_listeners.ContainsKey(button))
        {
            _listeners.Add(button, new List<Action>());
        }

        _listeners[button].Add(listener);
    }

    public void RemoveListener(KeyCode button, Action listener)
    {
        if (_listeners.ContainsKey(button))
        {
            _listeners[button].Remove(listener);
        }
    }

    void Update()
    {
        foreach (var key in _listeners.Keys)
        {
            if (Input.GetKeyDown(key))
            {
                _buttonActions.Clear();
                _buttonActions.AddRange(_listeners[key]);
                foreach (var buttonAction in _buttonActions)
                {
                    buttonAction?.Invoke();
                }
            }
        }
    }
}