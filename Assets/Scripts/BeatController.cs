﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BeatController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer beatMarker;
    [SerializeField] private SpriteRenderer circleBeatMarker;
    [SerializeField] private Transform sectors;
    [SerializeField] private Camera camera;
    [SerializeField] private Image vinette;
    [SerializeField] private Color normalColor;
    [SerializeField] private Color beatColor;
    [SerializeField] private int bpm;

    private float _beatDelay;
    private float _timer;
    private bool _active;

    public int Bpm
    {
        get => bpm;
        set
        {
            _active = true;
            bpm = value;
            _beatDelay = 60f / bpm;
            switch (bpm)
            {
                case 70:
                    sectors.localScale = Vector3.one * 2f;
                    camera.orthographicSize = 4;
                    break;
                case 100:
                    sectors.localScale = Vector3.one * 2.2f;
                    camera.orthographicSize = 4.5f;
                    break;
                case 115:
                    sectors.localScale = Vector3.one * 2.4f;
                    camera.orthographicSize = 5;
                    break;
                case 135:
                    sectors.localScale = Vector3.one * 2.6f;
                    camera.orthographicSize = 5.5f;
                    break;
            }
        }
    }

    public float BeatDelay => _beatDelay;
    public float FromLastBeat => _timer;

    private void Awake()
    {
        Core.Register(this);
    }

    private void Start()
    {
        _beatDelay = 60f / bpm;
    }

    private void Update()
    {
        if (!_active) return;
        _timer += Time.deltaTime;
        if (bpm > 0 && _timer >= _beatDelay && !Core.Get<EyesController>().SlowMo)
        {
            _timer = 0;
            beatMarker.DOColor(normalColor, BeatDelay / 2).From(beatColor).Play();
            circleBeatMarker.DOFade(0, BeatDelay / 2).From(1).Play();
            vinette.DOFade(0, BeatDelay / 2).From(1).Play();
            camera.DOOrthoSize(4, BeatDelay / 2).From(3.9f).Play();
        }
    }
}