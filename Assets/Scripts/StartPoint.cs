﻿using UnityEngine;

public class StartPoint : MonoBehaviour
{
    [SerializeField] private Transform _startPoint;
    [SerializeField] private Transform _endPoint;

    public Transform BeginPoint => _startPoint;

    public Transform EndPoint => _endPoint;
}