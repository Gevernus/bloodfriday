﻿using TMPro;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI[] scores;
    private int[] _scores = new int[4];

    private void Awake()
    {
        Core.Register(this);
    }

    public void AddScore(int type)
    {
        _scores[type] ++;
        scores[type].text = _scores[type].ToString();
    }
}