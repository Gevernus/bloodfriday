﻿using System.Collections.Generic;
using DG.Tweening;
using FMODUnity;
using UnityEngine;

public class SectorController : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private EventReference preHit;
    [SerializeField] private EventReference okHit;
    [SerializeField] private EventReference failHit;
    [SerializeField] private EventReference okPressed;
    [SerializeField] private EventReference failPressed;
    [SerializeField] private SpriteRenderer renderer;
    [SerializeField] private float offOpacity = 0.3f;
    [SerializeField] private float onOpacity = 0.6f;
    [SerializeField] private KeyCode button;
    [SerializeField] private Color okColor = Color.green;
    [SerializeField] private Color failColor = Color.red;
    [SerializeField] private Animator animator;

    private InputManager _inputManager;
    private readonly List<Enemy> _enemiesToKill = new List<Enemy>();
    private Color normalColor;
    private static readonly int Attack = Animator.StringToHash("Attack");
    private Player _player;

    private void Start()
    {
        _inputManager = Core.Get<InputManager>();
        _inputManager.AddListener(button, KillEnemy);
        normalColor = renderer.color;
        _player = Core.Get<Player>();
    }

    private void KillEnemy()
    {
        camera.DOShakeRotation(0.1f, 3, 50).Play();
        _player.ShowUI(false);
        Core.Get<PathController>().Run();
        animator.SetTrigger(Attack);
        RuntimeManager.PlayOneShot(preHit);
        if (_enemiesToKill.Count == 0)
        {
            RuntimeManager.PlayOneShot(failHit);
            RuntimeManager.PlayOneShot(failPressed);
            renderer.DOColor(normalColor, 0.3f).From(failColor).Play();
            return;
        }
        RuntimeManager.PlayOneShot(okHit);
        RuntimeManager.PlayOneShot(okPressed);
        renderer.DOColor(normalColor, 0.3f).From(okColor).Play();
        for (var i = 0; i < _enemiesToKill.Count; i++)
        {
            _enemiesToKill[i].Blow();
        }

        _enemiesToKill.Clear();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Enemy")) return;
        
        var enemy = other.GetComponent<Enemy>();
        enemy.Activated = true;
        _enemiesToKill.Add(enemy);
        if (!enemy.Activated)
        {
            var color = renderer.color;
            color.a = onOpacity;
            renderer.color = color; 
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.CompareTag("Enemy")) return;
        var enemy = other.GetComponent<Enemy>();
        _enemiesToKill.Remove(enemy);
        if (_enemiesToKill.Count > 0) return;
        var color = renderer.color;
        color.a = offOpacity;
        renderer.color = color;
    }

    public void Disable()
    {
        _inputManager.RemoveListener(button, KillEnemy);
    }
}