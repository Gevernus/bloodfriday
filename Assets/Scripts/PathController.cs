﻿using DG.Tweening;
using FMOD.Studio;
using FMODUnity;
using UnityEngine;
using STOP_MODE = FMOD.Studio.STOP_MODE;

public class PathController : MonoBehaviour
{
    [SerializeField] private Transform startPoint;
    [SerializeField] private PathPoints[] ways;
    [SerializeField] private float startDuration;
    [SerializeField] private EventReference doors;
    [SerializeField] private EventReference logo;
    [SerializeField] private EventReference mumble;
    [SerializeField] private GameObject logoObject;
    [SerializeField] private SpriteRenderer leftEyeObject;
    [SerializeField] private SpriteRenderer rightEyeObject;
    [SerializeField] private SpriteRenderer logoRenderer;

    private Player _player;
    private bool active;
    private EventInstance _logoInstance;
    private EventInstance _mumbleInstance;

    private Sequence _runSequence;

    private void Awake()
    {
        Core.Register(this);
    }

    private void Start()
    {
        _player = Core.Get<Player>();
        var sequence = DOTween.Sequence();
        sequence.AppendInterval(1.5f)
            .AppendCallback(() =>
            {
                _player.Run();
            })
            .Append(_player.transform.DOMove(startPoint.position, startDuration).SetEase(Ease.Linear))
            .OnComplete(WaitForAction).Play();
        DOTween.Sequence().AppendInterval(0.15f).AppendCallback(() =>
        {
            RuntimeManager.CreateInstance(doors).start();
            _logoInstance = RuntimeManager.CreateInstance(logo);
            _mumbleInstance = RuntimeManager.CreateInstance(mumble);
        }).Play();
    }

    private void WaitForAction()
    {
        _mumbleInstance.start();
        _player.Stop();
        _player.ShowUI(true);
    }

    public void Run()
    {
        if (active) return;
        Core.Get<ScoreController>().AddScore(3);
        _mumbleInstance.stop(STOP_MODE.IMMEDIATE);
        _logoInstance.start();
        logoObject.SetActive(true);
        Core.Get<SoundController>().PlayAmbience(true, 1);
        DOTween.Sequence().AppendInterval(3f).AppendCallback(() =>
        {
            logoRenderer.DOFade(0, 1f).OnComplete(() =>
            {
                leftEyeObject.DOFade(0, 1f).Play();
                rightEyeObject.DOFade(0, 1f).Play();
            }).Play();
            Core.Get<EnemySpawner>().StartSpawn();
            _player.Run();
            active = true;
            Run(ways[Random.Range(0, ways.Length)]);
        });
    }

    private void Run(PathPoints way)
    {
        _runSequence = DOTween.Sequence();
        foreach (var wayPoint in way.points)
        {
            _runSequence.Append(_player.transform.DOMove(wayPoint.position, way.pointDuration).SetEase(Ease.Linear));
        }

        _runSequence.Play();
    }

    public void Stop()
    {
        _runSequence.Kill();
    }
}