using System;
using UnityEngine;

[Serializable]
public class Player : MonoBehaviour
{
    [SerializeField] private float radius = 5;
    [SerializeField] private Transform flipTransform;
    [SerializeField] private SectorController[] sectors;
    [SerializeField] private Animator animator;

    private bool active;
    private static readonly int Run1 = Animator.StringToHash("Run");
    private static readonly int Stop1 = Animator.StringToHash("Stop");
    private static readonly int Stage = Animator.StringToHash("Stage");
    private static readonly int Smoke1 = Animator.StringToHash("Smoke");

    private void Awake()
    {
        Core.Register(this);
        animator.SetFloat(Stage, 1);
    }

    private void FixedUpdate()
    {
        var myPosition = transform.position;
        var closestEnemy = Core.Get<EnemySpawner>().GetClosestEnemy(myPosition);
        var enemyPosition = closestEnemy.transform.position;
        if ((enemyPosition - myPosition).sqrMagnitude <= radius * radius * 3)
        {
            var transformLocalScale = flipTransform.localScale;
            transformLocalScale.x = myPosition.x > enemyPosition.x ? -0.15f : 0.15f;
            flipTransform.localScale = transformLocalScale;
        }
    }

    public void Run()
    {
        animator.SetTrigger(Run1);
    }

    public void Stop()
    {
        animator.SetTrigger(Stop1);
    }

    public void Smoke()
    {
        animator.SetTrigger(Smoke1);
    }

    public void SetStage(int stage)
    {
        animator.SetFloat(Stage, stage);
    }

    public void ShowUI(bool part)
    {
        if (active) return;
        if (part)
        {
            sectors[0].gameObject.SetActive(true);
        }
        else
        {
            foreach (var sector in sectors)
            {
                sector.gameObject.SetActive(true);
            }

            active = true;
        }
    }

    public void DisableUI()
    {
        foreach (var sector in sectors)
        {
            sector.gameObject.SetActive(false);
            sector.Disable();
        }
    }
}