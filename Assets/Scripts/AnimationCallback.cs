﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AnimationCallback : MonoBehaviour
{
    public List<Action> onComplete = new List<Action>();

    public void InvokeCallback(int index)
    {
        if (index >= onComplete.Count) return;
        onComplete[index]?.Invoke();
    }
}