﻿using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField]
    private Player _player;

    private void Update()
    {
        var transformPosition = _player.transform.position;
        transformPosition.z = transform.position.z;
        transform.position = transformPosition;
    }
}