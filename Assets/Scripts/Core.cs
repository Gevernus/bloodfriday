using System;
using System.Collections.Generic;
public static class Core
{
    private static Dictionary<Type, object> systems = new Dictionary<Type, object>();
    public static T Get<T>()
    {
        if (systems.ContainsKey(typeof(T)))
        {
            return (T)systems[typeof(T)];
        }

        return default;
    }

    public static void Register<T>(T instance)
    {
        systems.Add(typeof(T), instance);
    }
}
