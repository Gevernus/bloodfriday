﻿using DG.Tweening;
using FMOD.Studio;
using FMODUnity;
using UnityEngine;
using STOP_MODE = FMOD.Studio.STOP_MODE;

public class SoundController : MonoBehaviour
{
    [SerializeField] private EventReference ambience;
    [SerializeField] private EventReference music;

    private EventInstance ambienceInstance;
    private EventInstance musicInstance;
    private bool _ambienceEnabled;
    private bool _musicEnabled;
    private bool _ambienceInited;
    private bool _musicInited;

    private void Awake()
    {
        Core.Register(this);
    }

    private void Start()
    {
        DOTween.Sequence().AppendInterval(0.5f).AppendCallback(() => { PlayAmbience(true, 0); }).Play();
    }

    public void PlayAmbience(bool enable, int index)
    {
        if (!_ambienceInited)
        {
            ambienceInstance = RuntimeManager.CreateInstance(ambience);
            _ambienceInited = true;
        }

        if (enable)
        {
            if (!_ambienceEnabled)
            {
                ambienceInstance.start();
            }

            ambienceInstance.setParameterByName("Game_State", index);
        }
        else
        {
            ambienceInstance.stop(STOP_MODE.ALLOWFADEOUT);
        }

        _ambienceEnabled = enable;
    }

    public void PlayMusic(bool enable, int index)
    {
        if (!_musicInited)
        {
            musicInstance = RuntimeManager.CreateInstance(music);
            _musicInited = true;
        }

        if (enable)
        {
            if (!_musicEnabled)
            {
                musicInstance.start();
            }

            musicInstance.setParameterByName("Phase_state", index);
        }
        else
        {
            musicInstance.stop(STOP_MODE.ALLOWFADEOUT);
        }

        _musicEnabled = enable;
    }
}