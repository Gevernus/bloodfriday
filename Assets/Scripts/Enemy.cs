﻿using System;
using DG.Tweening;
using FMOD.Studio;
using FMODUnity;
using UnityEngine;
using STOP_MODE = FMOD.Studio.STOP_MODE;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Transform flipTransform;
    [SerializeField] private Transform flipBloodTransform;
    [SerializeField] private Animator animator;
    [SerializeField] private EventReference running;
    [SerializeField] private EventReference killed;
    [SerializeField] private AnimationCallback callback;
    [SerializeField] private Collider2D coll;
    public bool Activated;
    public int Type;
    private Vector3 _startPosition;
    private Vector3 _endPosition;
    private float _duration;
    private Action<int> _onScore;
    private Sequence _sequence;
    private EventInstance runningEvent;
    private EventInstance killedEvent;
    private static readonly int Death = Animator.StringToHash("Death");

    public void Init(Vector2 startPoint, Vector2 endPoint, float duration, Action<int> addScore)
    {
        gameObject.SetActive(true);
        animator.enabled = true;
        _duration = duration;
        _startPosition = startPoint;
        _endPosition = endPoint;
        _onScore = addScore;
        killedEvent = RuntimeManager.CreateInstance(killed);
        runningEvent = RuntimeManager.CreateInstance(running);
        callback.onComplete.Add(StopMotion);
        callback.onComplete.Add(Clean);
        Run();
        coll.enabled = true;
    }

    private void Update()
    {
        runningEvent.set3DAttributes(transform.To3DAttributes());
    }

    private void Run()
    {
        runningEvent.start();
        _sequence = DOTween.Sequence();
        transform.position = _startPosition;
        _sequence
            .Append(transform.DOMove(new Vector3(_endPosition.x, _endPosition.y), _duration).SetEase(Ease.Linear))
            .OnComplete(Clean)
            .Play();
        var flipTransformLocalScale = flipTransform.localScale;
        flipTransformLocalScale.x = transform.position.x < Core.Get<Player>().transform.position.x ? -0.15f : 0.15f;
        flipTransform.localScale = flipTransformLocalScale;

        var localScale = flipBloodTransform.localScale;
        localScale.x = transform.position.x < Core.Get<Player>().transform.position.x ? -1f : 1f;
        flipBloodTransform.localScale = localScale;
    }

    public void Blow()
    {
        animator.enabled = true;
        runningEvent.stop(STOP_MODE.IMMEDIATE);
        animator.SetTrigger(Death);
        _onScore?.Invoke(Type);
        killedEvent.start();
        coll.enabled = false;
    }

    public void StopMotion()
    {
        _sequence.Kill();
    }

    private void Clean()
    {
        //Debug.Log($"Time: {Time.time - _time}");
        runningEvent.stop(STOP_MODE.IMMEDIATE);
        killedEvent.release();
        runningEvent.release();
        callback.onComplete.Clear();
        animator.enabled = false;
        _sequence.Kill();
        _sequence = null;
        gameObject.SetActive(false);
        Activated = false;
        _onScore = null;
        coll.enabled = false;
    }
}