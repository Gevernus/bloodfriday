﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class EyesController : MonoBehaviour
{
    [SerializeField] private Image[] stages;
    [SerializeField] private int[] bpms;
    [SerializeField] public SpriteRenderer blackPanel;

    public bool SlowMo;

    private int _index = -1;
    private bool waitToStop;

    private void Awake()
    {
        Core.Register(this);
    }

    public void SetStage(int index)
    {
        stages[index].DOFade(1, 2f).Play();
        if (_index != index)
        {
            _index = index;
            Core.Get<Player>().SetStage(index + 1);
            Debug.Log($"Play music {_index}");
            Core.Get<SoundController>().PlayMusic(true, _index);
            Core.Get<BeatController>().Bpm = bpms[_index];
            if (_index == 3)
            {
                SlowMo = true;
                DOTween.defaultTimeScaleIndependent = true;
                DOTween.Sequence()
                    .Append(DOTween.To(() => Time.timeScale, value => { Time.timeScale = value; }, 0.1f, 1f))
                    .Insert(0, blackPanel.DOFade(1, 5f))
                    //.AppendInterval(5f)
                    .Append(DOTween.To(() => Time.timeScale, value => { Time.timeScale = value; }, 1f, 1f))
                    .OnComplete(() =>
                    {
                        SlowMo = false;
                        waitToStop = true;
                        Core.Get<Player>().Stop();
                        Core.Get<PathController>().Stop();
                    })
                    .Play();
            }
        }
    }

    private void FixedUpdate()
    {
        if (waitToStop)
        {
            if (Core.Get<EnemySpawner>().CountEnemies() <= 8)
            {
                Core.Get<SoundController>().PlayMusic(true, 4);
                waitToStop = false;
                Core.Get<Player>().DisableUI();
                blackPanel.DOFade(0, 3f).Play();
                var beatController = Core.Get<BeatController>();
                DOTween.Sequence()
                    .AppendCallback(() => { beatController.Bpm = 115; })
                    .AppendInterval(2f)
                    .AppendCallback(() => { beatController.Bpm = 100; })
                    .AppendInterval(2f)
                    .AppendCallback(() => { beatController.Bpm = 70; })
                    .AppendInterval(2f)
                    .AppendCallback(() => { beatController.Bpm = 0; })
                    .AppendCallback(() =>
                    {
                        Core.Get<SoundController>().PlayAmbience(false, 0);
                        Core.Get<Player>().Smoke();
                    }).Play();
            }
        }
    }

    public void StopGame()
    {
    }
}